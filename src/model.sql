-- Удаление существующих таблиц перед созданием

DROP TABLE IF EXISTS items;

DROP TABLE IF EXISTS orders;

DROP TABLE IF EXISTS clients;

DROP TABLE IF EXISTS masters;

DROP TABLE IF EXISTS itemstype;

DROP TABLE IF EXISTS phone;

DROP TABLE IF EXISTS categories;

DROP TABLE IF EXISTS order_master_list;

-- Удаление ролей

DROP ROLE IF EXISTS operator_bd;

DROP ROLE IF EXISTS user_bd;

DROP ROLE IF EXISTS DBA;


-- Создание таблиц

CREATE TABLE phone (
  id SERIAL PRIMARY KEY,
  phone_number VARCHAR(12) NOT NULL UNIQUE,
  CONSTRAINT ch_phone_number CHECK (phone_number ~ '^\+7[0-9]{10}$')
);

CREATE TABLE clients (
  id SERIAL PRIMARY KEY,
  lastname VARCHAR(255) NOT NULL,
  name VARCHAR(255) NOT NULL,
  phone_id BIGINT NOT NULL,
  CONSTRAINT fk_clients_phone_id FOREIGN KEY (phone_id) REFERENCES phone(id) ON DELETE CASCADE
);

CREATE TABLE categories (
  id SERIAL PRIMARY KEY,
  category CHAR(50) NOT NULL UNIQUE
);

CREATE TABLE masters (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  experience INT NOT NULL CHECK (experience >= 0),
  categories_id BIGINT NOT NULL,
  phone_id BIGINT NOT NULL,
  salary DECIMAL(10, 2) NOT NULL CHECK (salary >= 16242),
  CONSTRAINT fk_masters_phone_id FOREIGN KEY (phone_id) REFERENCES phone(id) ON DELETE CASCADE,
  CONSTRAINT fk_masters_categories_id FOREIGN KEY (categories_id) REFERENCES categories(id)
);

CREATE TABLE orders (
  id SERIAL PRIMARY KEY,
  order_date DATE NOT NULL CHECK (order_date <= CURRENT_DATE),
  clients_id BIGINT NOT NULL,
  aim VARCHAR(255) NOT NULL,
  masters_id BIGINT NOT NULL,
  CONSTRAINT fk_orders_clients_id FOREIGN KEY (clients_id) REFERENCES clients(id) ON DELETE CASCADE,
  CONSTRAINT fk_orders_masters_id FOREIGN KEY (masters_id) REFERENCES masters(id) ON DELETE CASCADE
);

CREATE TABLE itemstype (
  id SERIAL PRIMARY KEY,
  type VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE items (
  id SERIAL PRIMARY KEY,
  itemstype_id BIGINT NOT NULL,
  price DECIMAL(10, 2) NOT NULL CHECK (price >= 0),
  orders_id BIGINT NOT NULL,
  CONSTRAINT fk_items_orders_id FOREIGN KEY (orders_id) REFERENCES orders(id) ON DELETE CASCADE,
  CONSTRAINT fk_items_itemstype_id FOREIGN KEY (itemstype_id) REFERENCES itemstype(id)
);

-- Наполнение таблиц данными

INSERT INTO phone (phone_number) VALUES
  ('+79123456789'),
  ('+79234567890'),
  ('+79345678901'),
  ('+79456789012'),
  ('+79567890123'),
  ('+79678901234'),
  ('+79789012345'),
  ('+79890123456'),
  ('+79901234567'),
  ('+79912345678');
  
INSERT INTO clients (lastname, name, phone_id) VALUES
  ('Иванов', 'Петр', 1),
  ('Смирнов', 'Алексей', 2),
  ('Кузнецов', 'Сергей', 3),
  ('Попов', 'Дмитрий', 4),
  ('Васильев', 'Андрей', 5),
  ('Петров', 'Михаил', 6),
  ('Соколов', 'Александр', 7),
  ('Михайлов', 'Иван', 8),
  ('Федоров', 'Владимир', 9),
  ('Морозов', 'Артем', 10);
  
INSERT INTO categories (category) VALUES
  ('Высшая категория'),
  ('Первая категория'),
  ('Вторая категория'),
  ('Третья категория'),
  ('Четвёртая категория'),
  ('Пятая категория'),
  ('Шестая категория'),
  ('Седьмая категория'),
  ('Восьмая категория'),
  ('Девятая категория');
  
INSERT INTO masters (name, experience, categories_id, phone_id, salary) VALUES
  ('Гришин П. Д.', 5, 1, 1, 20000.00),
  ('Титов Г. А.', 8, 2, 2, 22000.00),
  ('Сахаров А. А.', 10, 3, 3, 24000.00),
  ('Крючков К. М.', 3, 4, 4, 18000.00),
  ('Козин С. А.', 1, 5, 5, 21000.00),
  ('Широкова Е. Т.', 9, 6, 6, 23000.00),
  ('Еремина Д. А.', 7, 7, 7, 22000.00),
  ('Попова А. М.', 4, 8, 8, 80000.00),
  ('Шмелев Е. В.', 2, 9, 9, 17000.00),
  ('Сорокин М. А.', 1, 10, 10, 100000.00);
  
INSERT INTO orders (order_date, clients_id, aim, masters_id) VALUES
  ('2023-10-01', 1, 'Ремонт каблука', 5),
  ('2023-10-02', 2, 'Замена подошвы', 4),
  ('2023-10-03', 3, 'Реставрация кожи', 1),
  ('2023-10-04', 4, 'Расширение обуви', 9),
  ('2023-10-05', 5, 'Пошив ботинок на заказ', 7),
  ('2023-10-06', 6, 'Замена молнии на сапогах', 2),
  ('2023-10-07', 7, 'Чистка и полировка обуви', 6),
  ('2023-10-08', 8, 'Изготовление индивидуальной обуви', 3),
  ('2023-10-09', 9, 'Пошив слипонов', 10),
  ('2023-11-01', 1, 'Замена подошвы', 4),
  ('2023-10-10', 10, 'Проклеивание ботинок', 8);
  
INSERT INTO itemstype (type) VALUES
  ('Зимние сапоги'),
  ('Кроссовки'),
  ('Кеды'),
  ('Спортивная обувь'),
  ('Туфли'),
  ('Сандалии'),
  ('Ботинки'),
  ('Полуботинки'),
  ('Слипоны'),
  ('Мокасины');


INSERT INTO items (itemstype_id, price, orders_id) VALUES
  (5, 100.50, 1),
  (2, 75.25, 2),
  (7, 50.00, 3),
  (1, 120.75, 4),
  (10, 85.20, 5),
  (1, 110.00, 6),
  (3, 70.75, 7),
  (3, 4500.60, 8),
  (9, 115.00, 9),
  (1, 2347.00, 10);

--BEGIN;
--ALTER TABLE phone
--RENAME COLUMN phone_number TO contact_number;

--ALTER TABLE masters
--ADD COLUMN email VARCHAR(255);

--COMMIT;

--BEGIN;
-- Изменение опыта работы для мастера с id=1
--UPDATE masters
--SET experience = 6
--WHERE id = 1;

-- Изменение цены для товара с id=3
--UPDATE items
--SET price = 55.00
--WHERE id = 3;

--ROLLBACK;

--BEGIN;
-- Удаление столбца email из таблицы masters
--ALTER TABLE masters
--DROP COLUMN email;

--SAVEPOINT sp1;

-- Удаление заказа с id=5
--DELETE FROM orders
--WHERE id = 5;

--ROLLBACK TO SAVEPOINT sp1;

-- Удаление клиента с id=8 и связанных с ним записей
--DELETE FROM clients
--WHERE id = 8;

--COMMIT;


  
--CREATE TABLE order_master_list AS
--SELECT
  --o.id AS order_id,
  --o.order_date,
  --o.aim,
  --c.lastname AS client_lastname,
  --c.name AS client_name,
  --m.name AS master_name
--FROM orders o
--JOIN clients c ON o.clients_id = c.id
--JOIN masters m ON o.masters_id = m.id;

--CREATE OR REPLACE FUNCTION get_masters_by_experience_salary(
    --in_experience INT,
    --in_salary DECIMAL(10, 2)
--)
--RETURNS TABLE (
    --master_name VARCHAR(255),
    --experience INT,
    --salary DECIMAL(10, 2)
--)
--AS $$
--BEGIN
    --RETURN QUERY
    --SELECT m.name, m.experience, m.salary
    --FROM masters m
    --WHERE m.experience > in_experience AND m.salary < in_salary;
--END;
--$$ LANGUAGE plpgsql;

--CREATE OR REPLACE FUNCTION get_orders_by_client_and_date(
    --in_client_id BIGINT,
    --in_interval INTERVAL
--)
--RETURNS TABLE (
    --order_id BIGINT,
    --order_date DATE,
    --aim VARCHAR(255),
    --client_lastname VARCHAR(255),
    --client_name VARCHAR(255),
    --master_name VARCHAR(255)
--)
--AS $$
--BEGIN
    --RETURN QUERY
    --SELECT
        --o.id::BIGINT AS order_id,
        --o.order_date,
        --o.aim,
        --c.lastname AS client_lastname,
        --c.name AS client_name,
        --m.name AS master_name
    --FROM orders o
    --JOIN clients c ON o.clients_id = c.id
    --JOIN masters m ON o.masters_id = m.id
    --WHERE
        --o.clients_id = in_client_id AND
        --o.order_date < CURRENT_DATE - in_interval;
--END;
--$$ LANGUAGE plpgsql;

--CREATE OR REPLACE FUNCTION get_items_by_type_and_price(
    --in_item_type VARCHAR(255),
    --in_price DECIMAL(10, 2)
--)
--RETURNS TABLE (
    --item_id BIGINT,
    --item_type VARCHAR(255),
    --price DECIMAL(10, 2)
--)
--AS $$
--BEGIN
    --RETURN QUERY
    --SELECT it.id::BIGINT AS item_id, it.type AS item_type, i.price
    --FROM items i
    --JOIN itemstype it ON i.itemstype_id = it.id
    --WHERE it.type = in_item_type AND i.price > in_price;
--END;
--$$ LANGUAGE plpgsql;

-- Создание ролей
CREATE ROLE operator_bd;
CREATE ROLE user_bd;
CREATE ROLE DBA;

-- Назначение прав доступа для оператора БД
GRANT SELECT, INSERT, UPDATE, DELETE ON clients, masters, orders, items TO operator_bd;

-- Предоставление прав на использование последовательности clients_id_seq роли оператора БД
GRANT USAGE, SELECT ON SEQUENCE clients_id_seq TO operator_bd;

-- Назначение прав доступа для пользователя БД
GRANT SELECT ON clients, masters, orders, items TO user_bd;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO DBA;

-- Назначение прав доступа для DBA
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO DBA;
